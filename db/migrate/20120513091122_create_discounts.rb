class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.integer :user_id
      t.integer :product_id

      t.timestamps
    end
  end
end
