class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.integer :user_id
      t.integer :product_id

      t.timestamps
    end
  end
end
