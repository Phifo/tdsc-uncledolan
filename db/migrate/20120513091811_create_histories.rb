class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.integer :user_id
      t.integer :product_price
      t.integer :product_id
      t.integer :desc_porc

      t.timestamps
    end
  end
end
