# Load the rails application
require File.expand_path('../application', __FILE__)
require 'will_paginate'

Encoding.default_external = Encoding::UTF_8

# Initialize the rails application
Uncledolan::Application.initialize!

ENV['RAILS_ENV'] = "production"
