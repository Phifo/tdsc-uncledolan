class Shop < ActiveRecord::Base
  belongs_to :user
  belongs_to :product
  
  def percentprice(num)
    percent = (num * 20) / 100
    return num - percent.to_i
  end
end
