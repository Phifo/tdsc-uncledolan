class PaymentNotification < ActiveRecord::Base
  belongs_to :shop
  serialize :params
  #after_create :mark_shop_as_purchased

  #private

  #def mark_shop_as_purchased
  #  if status == "Completed"
  #    shop.update_attribute(:purchased_at, Time.now)
  #  end
  #end
end
