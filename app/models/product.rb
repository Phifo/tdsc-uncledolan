class Product < ActiveRecord::Base
  establish_connection(:external_product_db)
  set_table_name 'Productos'
  has_many :shops
  has_many :users, :through => :shops
  has_many :discounts
end
