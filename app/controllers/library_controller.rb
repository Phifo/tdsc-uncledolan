class LibraryController < ApplicationController
  def index
    @shops = Shop.where(:user_id => current_user.id)
    @histories = History.where(:user_id => current_user.id)
    @invoices = Invoice.where(:user_id => current_user.id, :used => true)
  end
end
