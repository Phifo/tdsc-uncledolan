class ProductsController < ApplicationController
  def index
    @products = Product.paginate(:page => params[:page], :per_page => 8)
  end

  def show
    @product = Product.find(params[:id])
    @shop = Shop.new
  end
end
