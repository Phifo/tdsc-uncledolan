class PaymentNotificationsController < ApplicationController
  protect_from_forgery :except => [:create]
  # GET /payment_notifications
  # GET /payment_notifications.json
  # POST /payment_notifications
  # POST /payment_notifications.json
  def create
    parametros = 0
    if params[:st] == "Completed"
      parametros = Hash.from_xml(RestClient.get('https://www.sandbox.paypal.com/cgi-bin/webscr', :cmd => '_notify-synch', :tx => params[:tx], :at => 'XKFKHGKGTPXV6'))
    end
    #PaymentNotification.create(:params => params, :shop_id => params[:invoice], :status => params[:payment_status], :transaction_id => params[:txn_id])
    #if parametros[:payment_status] == "Completed"
    #  invoice = Invoice.find(parametros[:invoice])
    #  invoice.used = true
    #  user_id = invoice.user_id

    #  items = Shop.where(:invoice_id => invoice.id)
  
    #  items.each do |item|
    #    history = History.new
    #    history.user_id = user_id
    #    history.product_name = item.product.Nombre
    #    history.product_price = item.product.Precio
    #    history.product_cod = item.product.Codigo
    #    history.invoice_id = invoice.id
    #    history.save
    #  end
  
    #  invoice.save
    #  items.destroy_all
    #end
    redirect_to shops_path
  end

#  def index
#    @payment_notifications = PaymentNotification.all
#
#    respond_to do |format|
#      format.html # index.html.erb
#      format.json { render json: @payment_notifications }
#    end
#  end
#
#  # GET /payment_notifications/1
#  # GET /payment_notifications/1.json
#  def show
#    @payment_notification = PaymentNotification.find(params[:id])
#
#    respond_to do |format|
#      format.html # show.html.erb
#      format.json { render json: @payment_notification }
#    end
#  end
#
#  # GET /payment_notifications/new
#  # GET /payment_notifications/new.json
#  def new
#    @payment_notification = PaymentNotification.new
#
#    respond_to do |format|
#      format.html # new.html.erb
#      format.json { render json: @payment_notification }
#    end
#  end
#
#  # GET /payment_notifications/1/edit
#  def edit
#    @payment_notification = PaymentNotification.find(params[:id])
#  end
#
#  
#
#  # PUT /payment_notifications/1
#  # PUT /payment_notifications/1.json
#  def update
#    @payment_notification = PaymentNotification.find(params[:id])
#
#    respond_to do |format|
#      if @payment_notification.update_attributes(params[:payment_notification])
#        format.html { redirect_to @payment_notification, notice: 'Payment notification was successfully updated.' }
#        format.json { head :no_content }
#      else
#        format.html { render action: "edit" }
#        format.json { render json: @payment_notification.errors, status: :unprocessable_entity }
#      end
#    end
#  end
#
#  # DELETE /payment_notifications/1
#  # DELETE /payment_notifications/1.json
#  def destroy
#    @payment_notification = PaymentNotification.find(params[:id])
#    @payment_notification.destroy
#
#    respond_to do |format|
#      format.html { redirect_to payment_notifications_url }
#      format.json { head :no_content }
#    end
#  end
end
