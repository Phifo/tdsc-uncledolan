class ShopsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :verify_invoice
  # GET /shops
  # GET /shops.json

  def express
    shops = Shop.where(:user_id => current_user.id)
    invoice = Invoice.find_by_user_id_and_used(current_user.id, false).id
    values = {
      :business => 'fernan_1338285827_biz@gmail.com',
      :cmd => '_cart',
      :upload => 1,
      :return => 'http://uncledolan.inf.utfsm.cl/payment_notifications',
      :invoice => invoice,
      #:notify_url => 'http://uncledolan.inf.utfsm.cl/payment_notifications'
    }
    shops.each_with_index do |shop, index|
      if shop.product.discounts.find_by_user_id(current_user.id) != nil
      values.merge!({      
        "amount_#{index+1}" => to_dolar(percentprice(shop.product.Precio)),
        "item_name_#{index+1}" => shop.product.Nombre,
        "item_number_#{index+1}" => shop.product.Codigo,
        "quantity_#{index+1}" => 1
      })
      else
      values.merge!({      
        "amount_#{index+1}" => to_dolar(shop.product.Precio),
        "item_name_#{index+1}" => shop.product.Nombre,
        "item_number_#{index+1}" => shop.product.Codigo,
        "quantity_#{index+1}" => 1
      })
      end
    end
     
    # This is a paypal sandbox url and should be changed for production.
    # Better define this url in the application configuration setting on environment
    # basis.
    redirect_to("https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query)
  end

  def index
    @shops = Shop.where(:user_id => current_user.id)
    @histories = History.where(:user_id => current_user.id)
    @invoices = Invoice.where(:user_id => current_user.id, :used => true)
    @discounts = Discount.where(:user_id => current_user.id)
    
    @total = 0
    @shops.each do |shop|
      if shop.product.discounts.find_by_user_id(current_user.id) != nil
        @total += percentprice(shop.product.Precio)
      else
        @total += shop.product.Precio
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @shops }
    end
  end

  # GET /shops/1
  # GET /shops/1.json
#  def show
#    @shop = Shop.find(params[:id])

#    respond_to do |format|
#      format.html # show.html.erb
#      format.json { render json: @shop }
#    end
#  end
#
#  # GET /shops/new
#  # GET /shops/new.json
#  def new
#    @shop = Shop.new
#
#    respond_to do |format|
#      format.html # new.html.erb
#      format.json { render json: @shop }
#    end
#  end
#
#  # GET /shops/1/edit
#  def edit
#    @shop = Shop.find(params[:id])
#  end
#
#  # POST /shops
#  # POST /shops.json
  def create
    invoice = Invoice.find_by_user_id_and_used(current_user.id, false)
    @shop = Shop.new(:user_id => params[:user_id], :product_id => params[:product_id], :invoice_id => invoice.id)

    respond_to do |format|
      if @shop.save
        format.html { redirect_to shops_path, notice: 'Shop was successfully created.' }
        format.json { render json: @shop, status: :created, location: @shop }
#      else
#        format.html { render action: "new" }
#        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end
#
#  # PUT /shops/1
#  # PUT /shops/1.json
#  def update
#    @shop = Shop.find(params[:id])
#
#    respond_to do |format|
#      if @shop.update_attributes(params[:shop])
#        format.html { redirect_to @shop, notice: 'Shop was successfully updated.' }
#        format.json { head :no_content }
#      else
#        format.html { render action: "edit" }
#        format.json { render json: @shop.errors, status: :unprocessable_entity }
#      end
#    end
#  end
#
#  # DELETE /shops/1
#  # DELETE /shops/1.json
  def destroy
    @shop = Shop.find(params[:id])
    @shop.destroy

    respond_to do |format|
      format.html { redirect_to shops_url }
      format.json { head :no_content }
    end
  end
  
  def to_dolar(num)
    return num = ("%.2f" % (num/510.25)).to_f
  end

  def verify_invoice
    if Invoice.find_by_user_id_and_used(current_user.id, false) == nil
      Invoice.create(:user_id => current_user.id, :used => false)
    end
  end

  def percentprice(num)
    percent = (num * 20) / 100
    return num - percent.to_i
  end
end
