class DiscountController < ApplicationController
  def adddiscount
    user_id = params[:user]
    product_id = params[:product]

    Discount.create(:user_id => user_id, :product_id => product_id)
    redirect_to product_path(product_id)
  end
end
